import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:navigation_drawer/new_page.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(new App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: defaultTargetPlatform == TargetPlatform.iOS
              ? Colors.grey[50]
              : null),
      debugShowCheckedModeBanner: false,
      home: NavigationTabs(),
      // routes: <String, WidgetBuilder>{
      //   "/a": (BuildContext context) => NewPage("New Page"),
      // }
    );
  }
}

class NavigationTabs extends StatefulWidget {
  @override
  _NavigationTabsState createState() => _NavigationTabsState();
}

class _NavigationTabsState extends State<NavigationTabs> {
  List<String> events = [
    "Calendar",
    "Family",
    "Friends",
    "Lovely",
    "Me",
    "Team"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("App"),
        elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Josimar Junior"),
              accountEmail: Text("josimarjr479@gmail.com"),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                    Theme.of(context).platform == TargetPlatform.iOS
                        ? Colors.blueGrey
                        : Colors.white,
                child: Text("J"),
              ),
              otherAccountsPictures: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Text("JR"),
                )
              ],
            ),
            ListTile(
              title: Text("Page one"),
              trailing: Icon(Icons.arrow_upward),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => NewPage("Page One")));
                //Navigator.of(context).pushNamed("/a");
              },
            ),
            ListTile(
              title: Text("Page two"),
              trailing: Icon(Icons.arrow_downward),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => NewPage("Page Two")));
                //Navigator.of(context).pushNamed("/a");
              },
            ),
            Divider(),
            ListTile(
              title: Text("Close"),
              trailing: Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/bg.png"), fit: BoxFit.cover),
        ),
        child: Container(
          margin: const EdgeInsets.only(top: 120),
          child: GridView(
            physics: BouncingScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2), // 2 item por linha
            children: events.map((title) {
              return GestureDetector(
                child: Card(
                  margin: const EdgeInsets.all(20),
                  child: getCardByTitle(title),
                ),
                onTap: () {
                  Fluttertoast.showToast(
                      msg: title,
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16);
                },
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  Column getCardByTitle(String title) {
    String img = "";
    if (title == "Calendar") {
      img = "assets/calendar.png";
    } else if (title == "Family") {
      img = "assets/family_time.png";
    } else if (title == "Friends") {
      img = "assets/friends.png";
    } else if (title == "Lovely") {
      img = "assets/lovely_time.png";
    } else if (title == "Me") {
      img = "assets/me_time.png";
    } else {
      img = "assets/team_time.png";
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Container(
            child: Stack(
              children: <Widget>[
                Image.asset(
                  img,
                  width: 80,
                  height: 80,
                ),
              ],
            ),
          ),
        ),
        Text(
          title,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        )
      ],
    );
  }
}

class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int selectIndex = 0;
  Color backgroundColor = Colors.white;

  List<NavigationItem> items = [
    NavigationItem(Icon(Icons.home), Text("home"), Colors.blue),
    NavigationItem(
        Icon(Icons.favorite_border), Text("Favorites"), Colors.pinkAccent),
    NavigationItem(Icon(Icons.search), Text("Search"), Colors.deepPurple),
    NavigationItem(Icon(Icons.person_outline), Text("Profile"), Colors.cyan),
  ];

  Widget _buildItem(NavigationItem item, bool isSelected) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      width: isSelected ? 125 : 50,
      height: double.maxFinite,
      padding: isSelected ? EdgeInsets.only(left: 16, right: 16) : null,
      decoration: isSelected
          ? BoxDecoration(
              color: item.color,
              borderRadius: BorderRadius.all(Radius.circular(50)))
          : null,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              IconTheme(
                data: IconThemeData(
                  size: 24,
                  color: isSelected ? backgroundColor : Colors.black,
                ),
                child: item.icon,
              ),
              isSelected
                  ? Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: isSelected
                          ? DefaultTextStyle.merge(
                              style: TextStyle(color: backgroundColor),
                              child: item.title,
                            )
                          : null,
                    )
                  : Container()
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      padding: EdgeInsets.only(left: 8, top: 4, bottom: 4, right: 8),
      decoration: BoxDecoration(color: backgroundColor, boxShadow: [
        BoxShadow(
          color: Colors.black12,
          blurRadius: 4,
        )
      ]),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: items.map((item) {
          var itemIndex = items.indexOf(item);
          return GestureDetector(
            onTap: () {
              setState(() {
                selectIndex = itemIndex;
              });
            },
            child: _buildItem(item, selectIndex == itemIndex),
          );
        }).toList(),
      ),
    );
  }
}

class NavigationItem {
  final Icon icon;
  final Text title;
  final Color color;

  NavigationItem(this.icon, this.title, this.color);
}

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("App"),
        elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Josimar Junior"),
              accountEmail: Text("josimarjr479@gmail.com"),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                    Theme.of(context).platform == TargetPlatform.iOS
                        ? Colors.blueGrey
                        : Colors.white,
                child: Text("J"),
              ),
              otherAccountsPictures: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Text("JR"),
                )
              ],
            ),
            ListTile(
              title: Text("Page one"),
              trailing: Icon(Icons.arrow_upward),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => NewPage("Page One")));
                //Navigator.of(context).pushNamed("/a");
              },
            ),
            ListTile(
              title: Text("Page two"),
              trailing: Icon(Icons.arrow_downward),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => NewPage("Page Two")));
                //Navigator.of(context).pushNamed("/a");
              },
            ),
            Divider(),
            ListTile(
              title: Text("Close"),
              trailing: Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
